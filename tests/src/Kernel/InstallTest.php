<?php

namespace Drupal\Tests\leaner_toolbar\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the module can be enabled.
 *
 * And that phpunit can be run.
 *
 * @group leaner_toolbar
 */
class InstallTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'toolbar',
    'leaner_toolbar',
  ];

  /**
   * Test that this module can be enabled.
   */
  public function testInstall() {
    // Assert something so we know things work.
    self::assertTrue(TRUE);
  }

}
